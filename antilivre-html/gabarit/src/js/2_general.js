var deleteLog = false;
var placeHolder = $('.verslehaut');

new fullpage('.contenu', {
	//options here
  v2compatible: true,
	autoScrolling:true,
  scrollingSpeed: 500,
  css3:true,
  easingcss3: 'ease-out',
  loopTop: true,
  loopBottom: true,
  sectionSelector: '.haiku',
  fixedElements: '.logo, .compteur, .message',

  onLeave: function(anchorLink,index, nextIndex, direction) {
    placeHolder.html('');
    placeHolder.append(index - 1);
  }
});

//methods
//fullpage_api.setAllowScrolling(false);

// Vers le haut et le bas
$(document).on('click', '.verslehaut', function(){
  fullpage_api.moveSectionUp();
});
$(document).on('click', '.verslebas', function(){
  fullpage_api.moveSectionDown();
});
