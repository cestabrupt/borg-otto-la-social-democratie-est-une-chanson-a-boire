---
author: "Otto Borg"
title: "La social-démocratie est une chanson à boire"
publisher: "Abrüpt"
date: "janvier 2019"
description: "Des haïkus qui se promènent en social-démocratie"
subject: "poésie, haïku, social-démocratie"
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0044-9'
rights: "ø 2019 abrüpt éditions, creative commons zero"
---

#

S’étoile fascisme

Mais l’écran croit en son île

Sans pluie ou du sang

#

Parlement d’errance

Où se retranchent des rentes

Parti représente

#

Loué soit le vote

Satisfait de mon achat

Suis-je le moderne

#

Quoi charbon sans mines

Gueule en noir de droite à l’autre

L’urbain qui la raille

#

Mon indignation

Quel miroir est indigné

Digne mon reflet

#

Citoyens s’insultent

L’esthétique démocrate

Sur réseau social

#

Rance capital

D’assurance maladie

Rien prime d’attente

#

Qui dit lui qu’il chôme

Au bout de la corde il cesse

De crise et pourrisse

#

Où d’elle retraite

De rires seuls ses mains vieilles

Toit silence en veille

#

S’en dort la campagne

Défaits fermiers ou troupeaux

Saignés en écho

#

Rabais tiers d’émoi

La moitié plus de prix moins

Jouissance d’achats

#

Moi qui j’atermoie

J’y siffle comme selfie

Pianote et m’ennuie

#

Spectacles des morts

Adore hésite et torpeur

Ou soldes des seuls

#

Marcher sur les mains

Loin l’invisible chemin

Miens les gains sans mise

#

Transe socialiste

Délocalise ton corps

Carne en lettre morte

#

Impôts pour nos joies

Nos jours redistribués

Grand soir bas de laine

#

Providence aide et

Faire taire une misère

En terre rentière

#

libéral l’esprit

Frontières ses rêveries

Ses biens sans humains

#

Cent pour cent santé

Ment ni fausse état finance

Gratuite la fosse

#

Prix d’un plein province

Seul samedi parking vide

Tente et l’asphyxie

#

Courbe bourse et d’angles

Cours se voue à sa stridence

Se pend à sa chute

#

Vil mépris de villes

Vomit son clos d’industrie

Puis feint plaine en peine

#

Brûle crépuscule

Son agricultrice en paix

Sur tempe arme ou dettes

#

Hagard pour savoir

Télévision évangile

L’autre en sacrifice

#

Distraire cités

Taire la haine qui traîne

Contrôle dégaine

#

Matière dernière

Produit dérivé transfert

Gré engrais à ferme

#

Vivres se facturent

Dématérialisent chairs

Ment bête à crédit

#

Soir courbe chômage

Voir manger journal l’image

Boire à leur noyade

#

Cent clandestins brûlent

Paysage sans travail

Sang fume ils affluent

#

Déveine d’usine

S’enchaîne foule dévote

S’achètent leurs votes

