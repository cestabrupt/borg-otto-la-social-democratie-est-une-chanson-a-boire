# ~/ABRÜPT/OTTO BORG/LA SOCIAL-DÉMOCRATIE EST UNE CHANSON À BOIRE/*

La [page de ce livre](https://abrupt.ch/otto-borg/la-social-democratie-est-une-chanson-a-boire/) sur le réseau.

## Sur le livre

Trinquons, cassons, fracassons nos verres au minimum salarial. Et puis, passons le balai. Social-démocratie, haïkus et table rase, pour un livre qui ne soigne pas la gueule de bois.

## Sur l'auteur

Post-punk, post-ouvrier, punk quand même, ouvrier encore. Otto Borg est né en Allemagne. Quelque part à l’ombre du mur, sous les nuées de Tchernobyl. Le reste est commun à son siècle. L’usine, tourneur sur métaux. Puis le chômage. Puis une reconversion. Puis menuisier. Un peu musicien aussi. Écrivain, toujours, pour survivre dans l’usine et dans le siècle.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est dédié au domaine public. Il est mis à disposition selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
